# 参数说明文件

## data 数据

| 字段                  | 类型      | 描述                                             |
| --------------------- | --------- | ------------------------------------------------ |
| socket                | Object    | socket 对象                                      |
| sender                | Object    | 发送者对象                                       |
| revicer               | Object    | 接收者对象                                       |
| infoTemplate          | Object    | 信息模板                                         |
| productDetail         | Object    | 商品详细信息                                     |
| browseCard            | Object    | 卡片信息内容                                     |
| msgTimer              | Object    | 信息发送定时器                                   |
| windowHeight          | Integer   | 窗口高度                                         |
| chatContentHeight     | Integer   | 聊天内容高度                                     |
| floatBottomHeight     | Integer   | 输入框底部高度                                   |
| scrollTop             | Integer   | 滚动条位置高度                                   |
| level                 | Integer   | 客服评分等级                                     |
| solveResult           | Integer   | 是否解决问题                                     |
| currentEasy           | Integer   | 当前快捷回复树状图 Id                            |
| pageIndex             | Integer   | 当前聊天内容页数                                 |
| pageSize              | Integer   | 当前聊天内容一页的条数                           |
| current_state         | Integer   | 当前工具栏 Id                                    |
| customerMessage       | String    | 客服留言内容                                     |
| thisQuestion          | String    | 本次问题                                         |
| sendInfo              | String    | 发送的聊天内容                                   |
| abutmentUrl           | String    | 工具栏对接页面的 url                             |
| customerImg           | String    | 用户头像                                         |
| serviceImg            | String    | 客服头像                                         |
| messageTip            | String    | 提示内容                                         |
| noCode                | Timestamp | 每次进入客服功能的时间戳                         |
| isSelectSession       | Boolean   | 是否选中会话                                     |
| expressionShow        | Boolean   | 是否显示表情                                     |
| lastSession           | Boolean   | 是否为全部会话内容                               |
| sendState             | Boolean   | 是否可以发送                                     |
| openImitateProduct    | Boolean   | 是否开启虚拟商品                                 |
| conversition          | Array     | 当前用户会话内容                                 |
| expressions           | Array     | 表情列表                                         |
| serviceTool           | Array     | 客服工具栏列表                                   |
| fastReplay            | Array     | 快捷回复内容                                     |
| imageList             | Array     | 聊天内容图片列表                                 |
| start		              | Array     | 评星列表  			                                  |

## infoTemplate 数据

| 字段          | 类型      | 描述                                                                 |
| ------------- | --------- | -------------------------------------------------------------------- |
| SendId        | Integer   | 发送者 Id                                                            |
| ReviceId      | Integer   | 接收者 Id                                                            |
| Content       | String    | 发送内容                                                             |
| Identity      | Integer   | 发送者身份：0 机器人，1 客服员，2.会员                               |
| Type          | Integer   | 信息类型 ：0 文本，1 图片，2 表情，3 商品卡片/订单卡片，4 机器人回复 |
| State         | Integer   | 信息发送状态                                                         |
| NoCode        | Timestamp | 发送者时间戳                                                         |
| OutTradeNo    | String    | 发送者 socketId                                                      |
| CreateDateUtc | String    | 发送信息时间                                                         |
| Title         | String    | 推送卡片-标题                                                        |
| Description   | String    | 推送卡片-描述                                                        |
| Label         | String    | 推送卡片-标签                                                        |
| Thumbnail     | String    | 推送卡片-图片                                                        |
| NoSend        | Boolean   | 卡片标题                                                             |

## sender、revicer 数据

| 字段        | 类型    | 描述          |
| ----------- | ------- | ------------- |
| id          | Integer | Id            |
| isService   | Integer | 是否客服      |
| name        | String  | 名称          |
| onlineState | Boolean | 在线状态      |
| outTradeNo  | Integer | 用户 socketId |
| source      | Integer | 来源          |
| mobile      | String  | 手机号        |
| nickName    | String  | 昵称          |
| cardNo      | String  | 卡号          |
| receptNum   | Integer | 接待用户数量  |
